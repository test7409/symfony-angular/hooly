# Hooly



## Contexte fonctionnel

La société Hooly a 7 emplacements disponibles pour les foodtrucks sauf le vendredi où elle n'en n'a que 6.
Chaque foodtruck ne peut venir qu'une fois par semaine.
Un foodtruck ne peut pas réserver pour le jour même et ni pour une date passée.


## Exercice n°1 

- Réaliser une API à destination des foodtrucks, leur permettant de réserver des places
- contrainte technique : utiliser PHP 8 et Symfony 5 

## Exercice n°2

- Réaliser une IHM WEB à destination des foodtrucks, leur permettant de réserver des places et de lister l'ensemble des places qu'ils ont réservé
- contraintes techniques : utiliser l'API réalisé à l'exercice n°1 et Angular (version >11). Pas de charte graphique imposée, le visuel n'est pas évalué dans cet exercice
